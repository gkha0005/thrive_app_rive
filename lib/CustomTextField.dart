import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttericon/font_awesome5_icons.dart';

import 'CustomShadowWidget.dart';

class CustomTextField extends StatelessWidget {
  final String _hintText;
  final int? _maxLength;
  final String _textfieldType;
  final String _allowedRegex;
  final TextEditingController _controller;
  final FocusNode _focusNode;
  final bool? _formValidity;
  final Color? _fillColor;
  final Widget? _suffixIcon;

  const CustomTextField({
    Key? key,
    required String hintText,
    required int? maxLength,
    required String textfieldType,
    required String allowedRegex,
    required TextEditingController controller,
    required FocusNode focusNode,
    bool? formValidity,
    Color? fillColor,
    Icon? suffixIcon,
  })  : _hintText = hintText,
        _maxLength = maxLength,
        _textfieldType = textfieldType,
        _allowedRegex = allowedRegex,
        _controller = controller,
        _focusNode = focusNode,
        _formValidity = formValidity,
        _fillColor = fillColor,
        _suffixIcon = suffixIcon,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return _textfieldType == "normal"
        ? CustomShadowWidget(
      customWidget: TextFormField(
        controller: _controller,
        maxLength: _maxLength,
        focusNode: _focusNode,
        textCapitalization: TextCapitalization.sentences,
        textInputAction: TextInputAction.done,
        // keyboardType: TextInputType.text,
        decoration: InputDecoration(
            fillColor: _fillColor != null ? _fillColor : Colors.white,
            filled: true,
            hintText: _hintText,
            errorStyle: Theme.of(context).textTheme.overline!.copyWith(
              color: Colors.redAccent,
              fontSize: ScreenUtil().setSp(10.5),
              fontWeight: FontWeight.w500,
            ),
            counterText: "",
            contentPadding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(20.0), horizontal: ScreenUtil().setWidth(20)),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
              borderSide: BorderSide(style: BorderStyle.none, width: 0),
            ),
            suffixIcon: _suffixIcon ?? null),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(_allowedRegex)),
        ],
        style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w400, letterSpacing: 1.2),
        scrollPadding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom + 40),
        validator: (val) {
          if (val!.isNotEmpty && val.length < 3) {
            return "Username must be at least 3 characters";
          }
        },
        onEditingComplete: () {
          FocusScope.of(context).unfocus();
          SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
        },
      ),
    )
        : _textfieldType == "email"
        ? CustomShadowWidget(
      customWidget: TextFormField(
        controller: _controller,
        maxLength: _maxLength,
        focusNode: _focusNode,
        textCapitalization: TextCapitalization.sentences,
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          fillColor: _fillColor != null ? _fillColor : Colors.white,
          filled: true,
          hintText: _hintText,
          errorStyle: Theme.of(context).textTheme.overline!.copyWith(
            color: Colors.redAccent,
            fontSize: ScreenUtil().setSp(10.5),
            fontWeight: FontWeight.w500,
          ),
          counterText: "",
          contentPadding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(20.0), horizontal: ScreenUtil().setWidth(20)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
            borderSide: BorderSide(style: BorderStyle.none, width: 0),
          ),
          suffixIcon: _formValidity == true
              ? Padding(
            padding: EdgeInsets.only(right: ScreenUtil().setWidth(18)),
            child: Icon(
              FontAwesome5.check,
              color: Colors.green,
              size: ScreenUtil().setWidth(20),
            ),
          )
              : null,
        ),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(_allowedRegex)),
        ],
        style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w400, letterSpacing: 1.2),
        scrollPadding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom * 0.65),
        validator: (val) {
          if (val!.isNotEmpty && ((!val.toLowerCase().trim().endsWith("@monash.edu")) && (!val.toLowerCase().trim().endsWith("@student.monash.edu")))) {
            return "Not a valid monash email";
          }
        },
        onEditingComplete: () {
          FocusScope.of(context).unfocus();
          SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
        },
      ),
    )
        : Container();
  }
}
