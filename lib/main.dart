import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_password_strength/flutter_password_strength.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rive/rive.dart';

import 'CustomShadowWidget.dart';
import 'CustomTextField.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final blueTextColor = const Color(0xFF2367A0);
  final blueColorPrimary = const Color(0xFF006DAE);
  final blueColorPrimaryLight = const Color(0xFF1F97DE);
  final blueColorPrimaryDark = const Color(0xFF006DAE);
  final blueColorAccent = const Color(0xFF75C0FD);
  final blueColorBg = const Color(0xFFEDF7FF);
  final colorAllText = const Color(0xFF5D7288);
  final colorSoftText = const Color(0xFF5D7288); /*needs to be depreciated as it will not pass Monash assessibility*/
  final colorCardWhite = const Color(0xFFFFFFFF);
  final whiteGradientTop = const Color(0xFFFFFFFF); /*to be used in dialogue box BG */
  final whiteGradientbottom = const Color(0xFFEFF1F5).withOpacity(0.74);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: () => MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: blueTextColor,
        primaryColorLight: blueColorPrimaryLight,
        primaryColorDark: blueColorPrimaryDark,
        highlightColor: blueColorAccent,
        // scaffoldBackgroundColor: blueColorBg,
        backgroundColor: blueColorBg,
        cardColor: blueColorPrimary,
        shadowColor: blueColorPrimary.withOpacity(0.36),
          splashFactory: NoSplash.splashFactory,
          splashColor: Colors.transparent,
          dividerColor: Colors.transparent,
          canvasColor: blueColorBg,
          buttonTheme: ButtonThemeData(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32.0)),
            buttonColor: blueColorPrimary,
            splashColor: Colors.transparent,
          ),
          iconTheme: IconThemeData(
            color: blueColorPrimaryDark,
          ),
          chipTheme: ChipThemeData(
            labelStyle: TextStyle(
              color: blueColorPrimary,
              // fontSize: bodyText2,
              fontWeight: FontWeight.w500,
            ),
            selectedColor: blueColorPrimary,
            secondarySelectedColor: blueColorPrimaryLight,
            disabledColor: blueColorPrimary.withOpacity(0.5),
            brightness: Brightness.dark,
            shadowColor: blueColorPrimary.withOpacity(0.36),
            secondaryLabelStyle: const TextStyle(
              color: Colors.white,
              // fontSize: bodyText2,
              fontWeight: FontWeight.w500,
            ),
            padding: const EdgeInsets.only(left: 5, right: 5),
            backgroundColor: Colors.white,
          ),
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
            // elevation: 10,
            backgroundColor: Colors.white,
            selectedIconTheme: IconThemeData(color: blueTextColor),
            unselectedIconTheme: IconThemeData(color: colorSoftText.withOpacity(0.7)),
            selectedItemColor: blueTextColor,
            unselectedItemColor: colorSoftText.withOpacity(0.7),
            selectedLabelStyle: const TextStyle(fontWeight: FontWeight.w600),
            unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.w500),
            // type: BottomNavigationBarType.fixed,
          ),
          cardTheme: CardTheme(
            elevation: 8,
            color: blueColorPrimary,
            shadowColor: blueColorPrimary.withOpacity(0.36),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
          ),
          textTheme: TextTheme(
            headline1: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(30.0), fontWeight: FontWeight.w700),
            headline2: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(25.0), fontWeight: FontWeight.w700),
            headline3: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(20.0), fontWeight: FontWeight.w700),
            headline4: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(18.0), fontWeight: FontWeight.w700),
            headline5: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(14.0), fontWeight: FontWeight.w700),
            headline6: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(12.0), fontWeight: FontWeight.w400),
            subtitle1: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(18.0), fontWeight: FontWeight.w500),
            subtitle2: TextStyle(color: colorAllText,  fontSize: ScreenUtil().setSp(16.0), fontWeight: FontWeight.w500),
            overline: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(12.0), fontWeight: FontWeight.w300),
            bodyText2: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(16.0), fontWeight: FontWeight.w400),
            bodyText1: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(14.0), fontWeight: FontWeight.w400),
            caption: TextStyle(color: colorAllText, fontSize: ScreenUtil().setSp(12.0), fontWeight: FontWeight.w500),
          )
        ),

        home: const MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Artboard? _riveArtboard;
  SMIInput<bool>? _characterHandWave;
  SMIInput<bool>? _check;
  SMIInput<double>? _eyeTracking;
  SMIInput<bool>? _hidePassword;
  SMIInput<bool>? _loginSuccess;
  SMIInput<bool>? _loginFailed;
  SMIInput<double>? _artboardIndex;

  late final GlobalKey<FormState> _formKey;

  late TextEditingController _usernameEditingController;
  late TextEditingController _emailEditingController;
  late TextEditingController _passwordEditingController;

  late FocusNode _usernameFieldFocus;
  late FocusNode _emailFieldFocus;
  late FocusNode _passwordFieldFocus;

  late bool _passwordVisible;
  late double _passwordStrength;


  @override
  void initState() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive, overlays: [SystemUiOverlay.bottom]);
    super.initState();
    _usernameEditingController = TextEditingController();
    _emailEditingController = TextEditingController();
    _passwordEditingController = TextEditingController();

    _usernameFieldFocus = FocusNode();
    _emailFieldFocus = FocusNode();
    _passwordFieldFocus = FocusNode();

    _passwordVisible = false;
    _passwordStrength = 0.0;

    _formKey = GlobalKey<FormState>();

    _usernameEditingController.addListener(() {
      setState(() {});
      if (_usernameFieldFocus.hasFocus) {
        _check?.value = true;
        _eyeTracking?.value = _usernameEditingController.text.length.toDouble() * 3;
      } else {
        _check?.value = false;
      }
    });
    _emailEditingController.addListener(() {
      setState(() {});
      if (_emailFieldFocus.hasFocus) {
        _check?.value = true;
        _eyeTracking?.value = _emailEditingController.text.length.toDouble() * 3;
      } else {
        _check?.value = false;
      }
    });
    _passwordEditingController.addListener(() {
      setState(() {});
    });


    rootBundle.load('assets/rive/app_character_v7.riv').then(
          (data) async {
        // Load the RiveFile from the binary data.
        final file = RiveFile.import(data);

        // The artboard is the root of the animation and gets drawn in the
        // Rive widget.
        final artboard = file.artboardByName("Character_hollow");
        var controller = StateMachineController.fromArtboard(artboard!, 'PasswordCheck');
        if (controller != null) {
          artboard.addController(controller);
          _characterHandWave = controller.findInput('Handwave');
          _check = controller.findInput('Check');
          _eyeTracking = controller.findInput('Look');
          _hidePassword = controller.findInput('Hidepass');
          _loginSuccess = controller.findInput("Success");
          _loginFailed = controller.findInput('NotCorrect');
          _artboardIndex = controller.findInput('Color');
        }
        setState(() {
          _riveArtboard = artboard;
          _artboardIndex?.value = 1;
        });
      },
    );

    // AppUtility().signOutUser(context, false);

    Future.delayed(Duration(seconds: 1), () {
      _characterHandWave?.value = true;
    });

  }

  togglePasswordCheck(bool status) {
    _hidePassword?.value = status;
  }

  @override
  Widget build(BuildContext context) {
    var _width = ScreenUtil().screenWidth;
    var _height = ScreenUtil().screenHeight;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Theme.of(context).backgroundColor,
        body: SafeArea(
            top: false,
            bottom: false,
            child: Container(
              width: _width,
              height: _height,
              padding: EdgeInsets.only(top: ScreenUtil().setHeight(50)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    flex: 2,
                    fit: FlexFit.loose,
                    child: SizedBox(
                        width: _width * 0.8,
                        height: _height * 0.1,
                        child: Text(
                          "'Thrive' is only for Monash University students",
                          style: Theme.of(context).textTheme.headline3!.copyWith(fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        )),
                  ),
                  Flexible(
                    flex: 9,
                    child: SizedBox(
                      height: _emailEditingController.text.length > 0 && _formKey.currentState!.validate() == true ? _height * 0.54 : _height * 0.48,
                      child: Stack(
                        clipBehavior: Clip.none,
                        alignment: Alignment.topCenter,
                        children: [
                          _riveArtboard == null
                              ? const SizedBox()
                              : Container(
                            height: _height * 0.29,
                            width: _width * 0.8,
                            margin: EdgeInsets.only(bottom: ScreenUtil().setHeight(20)),
                            // decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                            child: GestureDetector(
                              child: Rive(
                                artboard: _riveArtboard!,
                                fit: BoxFit.fitWidth,
                              ),
                              onTap: () {
                                _characterHandWave?.value = true;
                              },
                            ),
                          ),
                          Positioned(
                            top: _height * 0.23,
                            child: Container(
                              width: _width * 0.9,
                              constraints: BoxConstraints(minHeight: _height * 0.28, maxHeight: _height * 0.4),
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              clipBehavior: Clip.none,
                              child: Form(
                                autovalidateMode: AutovalidateMode.always,
                                key: _formKey,
                                child: ListView(
                                  shrinkWrap: true,
                                  padding: _passwordFieldFocus.hasFocus
                                      ? EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom * 0.58)
                                      : _emailFieldFocus.hasFocus
                                      ? EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom * 0.65)
                                      : EdgeInsets.only(bottom: 0),
                                  children: [
                                    CustomShadowWidget(
                                      customWidget: CustomTextField(
                                        hintText: 'Enter a username',
                                        maxLength: 12,
                                        textfieldType: "normal",
                                        allowedRegex: r'^ ?[A-Za-z0-9 ]*',
                                        controller: _usernameEditingController,
                                        focusNode: _usernameFieldFocus,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    CustomShadowWidget(
                                      customWidget: CustomTextField(
                                        hintText: 'Your monash student email',
                                        maxLength: 100,
                                        textfieldType: "email",
                                        allowedRegex: r'^[A-Za-z0-9@._-]*',
                                        controller: _emailEditingController,
                                        focusNode: _emailFieldFocus,
                                        formValidity: _emailEditingController.text.length > 0 ? _formKey.currentState?.validate() : false,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Visibility(
                                      visible: _emailEditingController.text.length > 0 && _formKey.currentState!.validate() == true ? true : false,
                                      child: CustomShadowWidget(
                                        customWidget: TextFormField(
                                          controller: _passwordEditingController,
                                          maxLength: 30,
                                          autocorrect: false,
                                          enableSuggestions: false,
                                          keyboardType: TextInputType.visiblePassword,
                                          focusNode: _passwordFieldFocus,
                                          textCapitalization: TextCapitalization.sentences,
                                          textInputAction: TextInputAction.done,
                                          obscureText: !_passwordVisible,
                                          obscuringCharacter: "*",
                                          decoration: InputDecoration(
                                            fillColor: Colors.white,
                                            filled: true,
                                            hintText: 'Set Password',
                                            counterText: "",
                                            contentPadding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(20.0), horizontal: ScreenUtil().setWidth(20)),
                                            border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(15.0),
                                              borderSide: BorderSide(style: BorderStyle.none, width: 0),
                                            ),
                                            suffixIcon: IconButton(
                                              padding: EdgeInsets.all(ScreenUtil().radius(20)),
                                              splashRadius: 1,
                                              icon: Icon(
                                                _passwordVisible ? Icons.visibility : Icons.visibility_off,
                                                color: Theme.of(context).primaryColor,
                                                size: ScreenUtil().radius(22),
                                              ),
                                              onPressed: () {
                                                setState(() {
                                                  _passwordVisible = !_passwordVisible;
                                                  togglePasswordCheck(_passwordVisible);
                                                });
                                              },
                                            ),
                                          ),
                                          style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w400, letterSpacing: 1.2),
                                          scrollPadding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                                          onEditingComplete: () {
                                            setState(() {
                                              _passwordStrength;
                                            });
                                            FocusScope.of(context).unfocus();
                                            SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
                                          },
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: _emailEditingController.text.length > 0 && _formKey.currentState!.validate() == true ? true : false,
                                      child: Container(
                                        padding: EdgeInsets.only(
                                          top: ScreenUtil().setHeight(10),
                                          // bottom: ScreenUtil().setHeight(10),
                                          left: ScreenUtil().setWidth(40),
                                          right: ScreenUtil().setWidth(40),
                                        ),
                                        child: FlutterPasswordStrength(
                                            password: _passwordEditingController.text,
                                            height: ScreenUtil().setHeight(8),
                                            radius: ScreenUtil().radius(10),
                                            strengthCallback: (strength) {
                                              _passwordStrength = strength;
                                            }),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: SizedBox(
                        width: _width * 0.85,
                        height: _height * 0.1,
                        child: _emailEditingController.text.length > 0 && _formKey.currentState!.validate() == true
                            ? RichText(
                          text: TextSpan(
                            text: 'Password should at-least 8 characters, 1 Special character and 1 letter. '
                                'For security reasons, please make sure your password is ',
                            style: Theme.of(context).textTheme.overline!.copyWith(fontWeight: FontWeight.w400),
                            children: <TextSpan>[
                              TextSpan(text: 'not same as your Monash email password.', style: Theme.of(context).textTheme.overline!.copyWith(fontWeight: FontWeight.bold)),
                            ],
                          ),
                        )
                            : RichText(
                          // textAlign: TextAlign.center,
                          text: TextSpan(
                            text: 'Enter your ',
                            style: Theme.of(context).textTheme.overline!.copyWith(fontWeight: FontWeight.w400),
                            children: <TextSpan>[
                              TextSpan(text: 'Monash email ', style: Theme.of(context).textTheme.overline!.copyWith(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: 'to received an activation code, We do not store any personal data and '
                                      'email is only required to prove authenticity of Monash email addresses'),
                            ],
                          ),
                        )),
                  ),
                  // Flexible(child: SizedBox(height: _height * 0.03)),
                  Flexible(
                    child: SizedBox(
                      height: _height * 0.05,
                      child: TextButton(
                        onPressed: () {
                          HapticFeedback.lightImpact();
                        },
                        child: Text(
                          "I already have an account",
                          style: Theme.of(context).textTheme.headline5!.copyWith(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: Row(
                      children: [
                        Spacer(),
                        _passwordStrength > 0.70
                            ? GestureDetector(
                          child: Container(
                            width: _width * 0.5,
                            height: ScreenUtil().setHeight(70),
                            padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(20)),

                            decoration: BoxDecoration(
                              color: Color(0xFF006DAE),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40),
                              ),
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              'Activate',
                              style: Theme.of(context).textTheme.subtitle1!.copyWith(color: Colors.white),
                            ),
                          ),
                          onTap: () {
                            HapticFeedback.lightImpact();
                          },
                        )
                            : SizedBox(height: ScreenUtil().setHeight(70)),
                      ],
                    ),
                  ),
                ],
              ),
            )));
  }

  @override
  void dispose() {
    super.dispose();
    _usernameEditingController.dispose();
    _emailEditingController.dispose();
    _passwordEditingController.dispose();
    _usernameFieldFocus.dispose();
    _emailFieldFocus.dispose();
    _passwordFieldFocus.dispose();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
  }
}
