import 'package:flutter/material.dart';

class CustomShadowWidget extends StatelessWidget {
  const CustomShadowWidget({
    Key? key,
    required this.customWidget,
  }) : super(key: key);

  final Widget customWidget;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: customWidget,
      decoration: BoxDecoration(
        boxShadow: [ // so here your custom shadow goes:
          BoxShadow(
            color: Theme.of(context).cardTheme.shadowColor!.withOpacity(0.08),
            spreadRadius: 2,
            blurRadius: 14,
            offset: Offset(0, 3),
          ),
        ],
      ),
    );
  }
}